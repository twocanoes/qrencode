# qrencode for macOS

qrencode is a command line tool for creating ASCII or .png qrcodes. It is a great way to output qrcodes for information from scripts and when setting up Macs to communicate serial numbers and specs.

This project is a script that will compile qrencode statically. This means that it can be run without installing any other libraries. It also makes it great to run from recovery or on Macs that you don't want to install all the supporting tools: just copy qrencode and run it!

## Usage
To download, go to [https://bitbucket.org/twocanoes/qrencode/downloads/](https://bitbucket.org/twocanoes/qrencode/downloads/) and download the latest .dmg file. Mount the disk image and copy the qrencode binary to the target Mac (or put in someplace that is in your path).

To get usage, run it without any arguments, like this:

	./qrencode

	qrencode version 4.1.1
	Copyright (C) 2006-2017 Kentaro Fukuchi
	Usage: qrencode [-o FILENAME] [OPTION]... [STRING]
	Encode input data in a QR Code and save as a PNG or EPS image.
	
	-h           display this message.
	--help       display the usage of long options.
	-o FILENAME  write image to FILENAME. If '-' is specified, the result
	will be output to standard output. If -S is given, structured
	symbols are written to FILENAME-01.png, FILENAME-02.png, ...
	(suffix is removed from FILENAME, if specified)
	-r FILENAME  read input data from FILENAME.
	-s NUMBER    specify module size in dots (pixels). (default=3)
	-l {LMQH}    specify error correction level from L (lowest) to H (highest).
	(default=L)
	-v NUMBER    specify the minimum version of the symbol. (default=auto)
	-m NUMBER    specify the width of the margins. (default=4 (2 for Micro))
	-d NUMBER    specify the DPI of the generated PNG. (default=72)
	-t {PNG,PNG32,EPS,SVG,XPM,ANSI,ANSI256,ASCII,ASCIIi,UTF8,UTF8i,ANSIUTF8,ANSIUTF8i,ANSI256UTF8}
	specify the type of the generated image. (default=PNG)
	-S           make structured symbols. Version number must be specified with '-v'.
	-k           assume that the input text contains kanji (shift-jis).
	-c           encode lower-case alphabet characters in 8-bit mode. (default)
	-i           ignore case distinctions and use only upper-case characters.
	-8           encode entire data in 8-bit mode. -k, -c and -i will be ignored.
	-M           encode in a Micro QR Code.
	-V           display the version number and copyrights of the qrencode.
	[STRING]     input data. If it is not specified, data will be taken from
	standard input.
	
	Try "qrencode --help" for more options.
	
## Examples

### Show a URL
	./qrencode -t ANSI -o - "https://twocanoes.com"

![url terminal output](https://tc-assets.s3.amazonaws.com/qrencode/url.png)


### Generate a dad joke in a qrcode
	curl -H "Accept: text/plain" https://icanhazdadjoke.com/ | ./qrencode  -t ANSI

![joke terminal output](https://tc-assets.s3.amazonaws.com/qrencode/joke.png)

### Show a QR Code of whatever you last copied

	pbpaste | ./qrencode -t ANSI
	

### Show a QR code of the current macOS

	model_id=$(sysctl -n hw.model) 
	cpu=$(sysctl machdep.cpu.brand_string | sed "s/.*: \(.*\)/\1/g"|sed "s/Intel.*(TM) //g"| sed "s/ CPU @ /-/g") 
	hardware_memory=$(($(sysctl -n hw.memsize)/1024/1024/1024))
	first_physical_disk_size=$(diskutil list physical|grep 'GUID_partition_scheme'| head -1 |sed "s/.*GUID_partition_scheme.*\([0-9]\{1,\}\.[0-9]\{1,\}.*[a-z|A-Z]\{1,\}\).*disk.*/\1/g")
	
	mac_info=$(echo $model_id \| $cpu \| $hardware_memory GB \| $first_physical_disk_size )
	echo $mac_info
	# sample output: MacBookPro16,1 | i9-9880H-2.30GHz | 16 GB | 1.0 TB
	
	echo $mac_info | ./qrencode -t ANSI

![mac info terminal output](https://tc-assets.s3.amazonaws.com/qrencode/mac_info.png)
