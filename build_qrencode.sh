#!/bin/bash
 
set -e 
root_dir=$(pwd)
build_dir="${root_dir}/build"
products="${build_dir}"/products
products_qrencode="${products}"/qrencode
libpng_root="${products}"/libpng
DEV_ID="Developer ID Application: Twocanoes Software, Inc. (UXP6YEHSPW)"

mkdir -p "${products}"

cd "${build_dir}"

git clone https://github.com/glennrp/libpng.git

cd libpng



CFLAGS="-arch arm64 -arch x86_64" ./configure --enable-static --prefix="${libpng_root}" --host=aarch64-apple-darwin --target=aarch64-apple-darwin --build=aarch64-apple-darwin
make clean
make

make install

cd "${build_dir}"

git clone https://github.com/fukuchi/libqrencode.git

cd libqrencode

autoreconf -ivf   
CFLAGS="-arch arm64 -arch x86_64" ./configure  LDFLAGS="-all-static" PKG_CONFIG_PATH="${libpng_root}"/lib/pkgconfig --host=aarch64-apple-darwin --target=aarch64-apple-darwin --build=aarch64-apple-darwin
make clean
make 

mkdir "${products_qrencode}"
cp qrencode "${products_qrencode}"
cp "${build_dir}"/libpng/LICENSE "${products_qrencode}"/LICENSE_libpng.txt
cp "${build_dir}"/libqrencode/README "${products_qrencode}"/LICENSE_libqrencode.txt

codesign --verbose --force -o runtime --sign "${DEV_ID}" "${products_qrencode}"/qrencode

echo "qrencode is now in ${products_qrencode}. Enjoy!"

"${products_qrencode}"/qrencode -t ASCII "https://twocanoes.com" -o -

cd "${root_dir}"